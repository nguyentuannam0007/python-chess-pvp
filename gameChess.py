import pygame
import sys, os
import help_function as hf
from settings import Settings
from board import Board
from king import King
import threading
from threading import Lock

lock = Lock()
WHITE = 'w'
BLACK = 'b'
playerTurn = WHITE
winner = None

def input_move(board, screen):
    global playerTurn
    with lock:
        while not board.isGameOver():
            move = input('Enter your move: ')
            curpos, despos = hf.convert(move)
            if hf.isInBound(board.map_board, curpos[0], curpos[1]):
                temp = board.map_board[curpos[1]][curpos[0]]
                if temp != None and temp.color != playerTurn:
                    print('Not your turn !')
                    continue
            if board.hastoMove(curpos[0], curpos[1], despos[0], despos[1]):
                board.draw(screen)
                board.updateSquareCanMove()
                if playerTurn == WHITE: playerTurn = BLACK
                elif playerTurn == BLACK: playerTurn = WHITE
            else:
                print('Invalid move')

def game():
    global playerTurn

    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.width_screen, ai_settings.height_screen))
    screen.fill((255, 255, 255))
    pygame.display.set_caption('Chess')

    board = Board(ai_settings)
    board.init(screen)

    piecePickUp = None


    # Draw columns
    font = pygame.font.SysFont(None, 25)
    for i in range(8):
        text = str(chr(97 + i))
        text_x = ai_settings.width_piece * 1.5 + i * ai_settings.width_piece * 1
        text_y = ai_settings.height_piece * 8 + ai_settings.height_piece / 2
        text_image = font.render(text, True, (255, 0, 0))
        screen.blit(text_image, (text_x, text_y))
    # Draw ranks
    for i in range(8, 0, -1):
        text = str(i)
        text_x = ai_settings.width_piece / 2
        text_y = ai_settings.height_piece * (8 - i) + ai_settings.height_piece / 2
        text_image = font.render(text, True, (255, 0, 0))
        screen.blit(text_image, (text_x, text_y))

    input_thread = threading.Thread(target=input_move,
                                    args=[board, screen],
                                    daemon=True)
    input_thread.start()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONUP:
                mouse = pygame.mouse.get_pos()
                x_pos = (mouse[0] - ai_settings.width_piece) // ai_settings.width_piece
                y_pos = (mouse[1]) // ai_settings.height_piece
                tempPiece = None
                if hf.isInBound(board.map_board, x_pos, y_pos):
                    tempPiece = board.map_board[y_pos][x_pos]
                # Check game checkmate? If true, congratulations winner
                # Else
                # Player who has a turn can be pickUp piece
                #   If the king is checked -> piece has to move right thing to protect the king
                #   else moving piece in available square

                if piecePickUp == None:
                    if tempPiece != None:
                        piecePickUp = tempPiece
                        board.drawAvaiableSquare(screen, piecePickUp)
                else:
                    if piecePickUp.color == playerTurn:
                        if board.hastoMove(piecePickUp.x_pos, piecePickUp.y_pos, x_pos, y_pos):
                            board.updateSquareCanMove()
                            if playerTurn == WHITE:
                                playerTurn = BLACK
                            else:
                                playerTurn = WHITE
                    piecePickUp = None
                    board.draw(screen)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    king = None
                    for row in board.map_board:
                        for ele in row:
                            if ele != None and isinstance(ele, King) and ele.color == 'b':
                                king = ele
                    board.castling(king, 'r')
                    board.draw(screen)
        if board.isGameOver():
            print('Winner is ' + str(winner))

        pygame.display.flip()
    return board, screen

class Game:
    def __init__(self):
        pygame.init()
        self.ai_settings = Settings()
        self.board = Board(self.ai_settings)
        self.playerturn = WHITE
        self.piecePickUp = None
        self.screen = pygame.display.set_mode((self.ai_settings.width_screen,
                                                        self.ai_settings.height_screen))
        self.font = pygame.font.SysFont(None, 25)
        self.title = 'Chess'

    def drawColumns(self):
        # Draw columns
        font = pygame.font.SysFont(None, 25)
        for i in range(8):
            text = str(chr(97 + i))
            text_x = self.ai_settings.width_piece * 1.5 + i * self.ai_settings.width_piece * 1
            text_y = self.ai_settings.height_piece * 8 + self.ai_settings.height_piece / 2
            text_image = font.render(text, True, (255, 0, 0))
            self.screen.blit(text_image, (text_x, text_y))
    def drawRanks(self):
        for i in range(8, 0, -1):
            text = str(i)
            text_x = self.ai_settings.width_piece / 2
            text_y = self.ai_settings.height_piece * (8 - i) + self.ai_settings.height_piece / 2
            text_image = self.font.render(text, True, (255, 0, 0))
            self.screen.blit(text_image, (text_x, text_y))


    def draw(self):

        pygame.display.set_caption(self.title)
        self.screen.fill((255,255,255))
        self.board.init(self.screen)
        self.drawColumns()
        self.drawRanks()

    def start(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONUP:
                    mouse = pygame.mouse.get_pos()
                    x_pos = (mouse[0] - self.ai_settings.width_piece) // self.ai_settings.width_piece
                    y_pos = (mouse[1]) // self.ai_settings.height_piece
                    tempPiece = None
                    if hf.isInBound(self.board.map_board, x_pos, y_pos):
                        tempPiece = self.board.map_board[y_pos][x_pos]
                    # Check game checkmate? If true, congratulations winner
                    # Else
                    # Player who has a turn can be pickUp piece
                    #   If the king is checked -> piece has to move right thing to protect the king
                    #   else moving piece in available square

                    if self.piecePickUp == None:
                        if tempPiece != None:
                            self.piecePickUp = tempPiece
                            self.board.drawAvaiableSquare(self.screen, self.piecePickUp)
                    else:
                        if self.piecePickUp.color == self.playerturn:
                            if self.board.hastoMove(self.piecePickUp.x_pos, self.piecePickUp.y_pos, x_pos, y_pos):
                                self.board.updateSquareCanMove()
                                if self.playerturn == WHITE:
                                    self.playerturn = BLACK
                                else:
                                    self.playerturn = WHITE
                        self.piecePickUp = None
                        self.board.draw(self.screen)
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        king = None
                        for row in self.board.map_board:
                            for ele in row:
                                if ele != None and isinstance(ele, King) and ele.color == 'b':
                                    king = ele
                        self.board.castling(king, 'r')
                        self.board.draw(self.screen)
            if self.board.isGameOver():
                print('Winner is ' + str(winner))

            pygame.display.flip()


game = Game()
game.draw()
game.start()