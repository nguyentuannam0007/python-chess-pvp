import pygame
import help_function as hf
from piece import Piece

class Pawn(Piece):
    def __init__(self, ai_settings, color='white'):
        super().__init__(ai_settings)
        if color.lower() == 'white':
            self.color = 'w'
            self.direction = -1
            self.enpassingRow = 3
            self.image = pygame.image.load('images\\pieces\\w-pawn.png')
        elif color.lower() == 'black':
            self.color = 'b'
            self.direction = 1
            self.enpassingRow = 4
            self.image = pygame.image.load('images\\pieces\\b-pawn.png')
        self.rect = self.image.get_rect()
        self.rect.w = ai_settings.width_piece
        self.rect.h = ai_settings.height_piece

        self.image = pygame.transform.smoothscale(self.image, (ai_settings.width_piece, ai_settings.height_piece))

        # Boolean for check firstMove
        self.count_move = 0
        self.isEnpassing = False
        # Squares in enpassing Pawn
        self.squareEnpassing = []
    def findSquaresCanMove(self, board):
        self.squaresCanMove.clear()
        self.squareEnpassing.clear()
        square = [self.x_pos, self.y_pos + self.direction]
        # Find square can move straight forward
        if hf.isInBound(board.map_board, square[0], square[1]) and board.map_board[square[1]][square[0]] == None:
            self.squaresCanMove.append(square.copy())
        square[1] += self.direction
        is_first_step = len(self.squaresPassOver) == 0
        if is_first_step and hf.isInBound(board.map_board, square[0], square[1]) and board.map_board[square[1]][square[0]] == None and board.map_board[square[1]][square[0]] == None:
            self.squaresCanMove.append(square.copy())
        # Find square can move diagonal forward
        squares = [[self.x_pos - 1, self.y_pos + self.direction], [self.x_pos + 1, self.y_pos + self.direction]]
        for square in squares:
            if hf.isInBound(board.map_board, square[0], square[1]):
                if board.map_board[square[1]][square[0]] != None and board.map_board[square[1]][square[0]].color != self.color:
                    self.squaresCanMove.append(square)
        # Enpassing
        if self.y_pos == self.enpassingRow:
            for dx in range(-1,2,2):
                if self.x_pos + dx > -1 and self.x_pos + dx < len(board.map_board):
                    piece = board.map_board[self.y_pos][self.x_pos+dx]
                    if isinstance(piece, Pawn):
                        piece == board.last_piece
                        if piece.squaresPassOver[-1] == [piece.x_pos, piece.y_pos - 2*piece.direction]:
                            self.squaresCanMove.append([piece.x_pos, piece.y_pos - piece.direction])
                            self.squareEnpassing.append([piece.x_pos, piece.y_pos - piece.direction])


