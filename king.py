import pygame
import help_function as hf
from piece import Piece

class King(Piece):
    def __init__(self, ai_settings, color='white'):
        super().__init__(ai_settings)
        if color.lower() == 'white':
            self.color = 'w'
            self.dir_right = 2
            self.dir_left = -2
            self.image = pygame.image.load('images\\pieces\\w-king.png')
        elif color.lower() == 'black':
            self.color = 'b'
            self.dir_right = -2
            self.dir_left = 2
            self.image = pygame.image.load('images\\pieces\\b-king.png')
        self.rect = self.image.get_rect()
        self.rect.w = ai_settings.width_piece
        self.rect.h = ai_settings.height_piece

        self.image = pygame.transform.smoothscale(self.image, (ai_settings.width_piece, ai_settings.height_piece))

        # square for castling
        self.squaresCastling = []

    def findSquaresCanMove(self, board):
        self.squaresCanMove.clear()
        self.squaresCastling.clear()
        x_pos = self.x_pos
        y_pos = self.y_pos
        temp_squares = [[x_pos - 1, y_pos - 1], [x_pos, y_pos - 1], [x_pos + 1, y_pos - 1],
                       [x_pos - 1, y_pos], [x_pos + 1, y_pos],
                       [x_pos - 1, y_pos + 1], [x_pos, y_pos + 1], [x_pos + 1, y_pos + 1]
                       ]
        for square in temp_squares:
            if not hf.isInBound(board.map_board, square[0], square[1]):
                continue
            if board.map_board[square[1]][square[0]] != None:
                if board.map_board[square[1]][square[0]].color != self.color:
                    self.squaresCanMove.append(square)
            else:
                self.squaresCanMove.append(square)

        # Find square can castling

        if board.canCastling(self, 'r'):
            self.squaresCanMove.append([self.x_pos + self.dir_right, self.y_pos])
            self.squaresCastling.append([self.x_pos + self.dir_right, self.y_pos])
        if board.canCastling(self, 'l'):
            print('I am never in this')
            self.squaresCanMove.append([self.x_pos + self.dir_left, self.y_pos])
            self.squaresCastling.append([self.x_pos + self.dir_left, self.y_pos])




