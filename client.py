import socket
import sys
import gameChess
import help_function as hf
import tincanchat
import threading
import pygame
from settings import Settings
from board import Board
from gameChess import Game
from threading import Lock

HOST = '127.0.0.1'
PORT = 1234
lock = Lock()

WHITE = 'w'
BLACK = 'b'
playerTurn = WHITE
winner = None
piecePickUp = None
colorChess = None


def handle_move(move, board, screen):
    global colorChess, playerTurn
    canMove = False
    curpos, despos = hf.convert(move)
    if hf.isInBound(board.map_board, curpos[0], curpos[1]):
        temp = board.map_board[curpos[1]][curpos[0]]
        if temp == None:
            return False
        if temp != None and temp.color != playerTurn:
            print('Not your turn')
            return False
        if temp != None and temp.color != colorChess:
            print('Your color chess is:', colorChess)
            return False

    if board.hastoMove(curpos[0], curpos[1], despos[0], despos[1]):
        board.draw(screen)
        board.updateSquareCanMove()
        if playerTurn == WHITE:
            playerTurn = BLACK
        elif playerTurn == BLACK:
            playerTurn = WHITE
        canMove = True

    return canMove

def input_move(sock, board, screen):
    while not board.isGameOver():
        move = input('Enter your move: ')
        if handle_move(move, board, screen):
            # Send move from other side
            print('Send message: ', move)
            tincanchat.send_msg(sock, move)
        else:
            print('Invalid move')
    print('Game is over')


def handle_recv(sock, board, screen, addr):
    while True:
        try:
            msg = tincanchat.recv_msg(sock, addr)
            if msg != 'not your turn':
                move_chess(msg, board, screen)
            else:
                print(msg)

        except ConnectionError:
            print('Client {} disconnected'.format(addr))
            break

def move_chess(move, board, screen):
    global playerTurn
    # Valid move from opposite
    curpos, despos = hf.convert(move)
    print(board.hastoMove(curpos[0], curpos[1], despos[0], despos[1]))
    board.draw(screen)
    board.updateSquareCanMove()
    if playerTurn == WHITE: playerTurn = BLACK
    elif playerTurn == BLACK: playerTurn = WHITE

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_sock:
    client_sock.connect((HOST, PORT))

    # Set up color to server
    colorChess = input('Enter your color chess (BLACK or WHITE): ')
    playername = input('Enter your name: ')

    if colorChess.upper() == 'WHITE':
        colorChess = WHITE
    elif colorChess.upper() == 'BLACK':
        colorChess = BLACK
    tincanchat.send_msg(client_sock, colorChess)

    #Set up game
    chess = Game()
    chess.title = playername
    chess.draw()

    input_thread = threading.Thread(target=input_move,
                                    args=[client_sock, chess.board, chess.screen],
                                    daemon=True)

    recv_thread = threading.Thread(target=handle_recv,
                                   args=[client_sock, chess.board, chess.screen, client_sock.getsockname()],
                                   daemon=True)

    input_thread.start()
    recv_thread.start()

    chess.start()




