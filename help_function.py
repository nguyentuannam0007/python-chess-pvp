import pygame
import sys, os

def check_events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()



def swapTwoPiece(map_board, xA, yA, xB, yB):
    temp = map_board[xA][yA]
    map_board[xA][yA] = None
    map_board[xB][yB] = temp

def swapPawnEnPassing(map_board, xA, yA, xB, yB, direction):
    temp = map_board[xA][yA]
    map_board[xA][yA] = None
    map_board[xB][yB] = temp
    map_board[xB-direction][yB] = None

# Find square in the specifical diagonal line
def findSquaresinLineVector(map_board, piece, delx, dely):
    squaresCanMove = []
    square = [piece.x_pos + delx, piece.y_pos + dely]
    while True:
        if square[0] < len(map_board) and square[1] < len(map_board) and square[0] > -1 and square[1] > -1:
            if map_board[square[1]][square[0]] != None:
                if map_board[square[1]][square[0]].color != piece.color:
                    squaresCanMove.append(square.copy())
                break
            else:
                    squaresCanMove.append(square.copy())
            square[0] += delx
            square[1] += dely
        else:
            break
    return squaresCanMove

def findSquaresRankTwoPiece(map_board, piOne, piTwo, dx):
    # Find path from piece One to pieceTwo
    squares = []
    piOne_pos = piOne.getPosition()
    piTwo_pos = piTwo.getPosition()
    x = dx
    while True:
        square = [piOne_pos[0] + x, piOne_pos[1]]
        if not isInBound(map_board, square[0], square[1]) or square[0] == piTwo_pos[0]:
            break
        squares.append(square)
        x += dx

    return squares

def isInBound(map_board, x, y):
    if x > -1 and x < len(map_board) and y > -1 and y < len(map_board):
        return True
    return False


def convert(move):
    _from, _to = move.split('-')
    return convertPos(_from), convertPos(_to)

def convertPos(pos):
    return [ord(pos[0]) - 97, 8 - int(pos[1])]
