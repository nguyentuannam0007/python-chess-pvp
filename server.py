#!/usr/bin/env python3

import socket
import tincanchat
import threading
import queue

from time import ctime

HOST = '127.0.0.1'
PORT = 1234
BUFSIZ = 1024

WHITE = 'w'
BLACK = 'b'

lock = threading.Lock()
clients = {}
send_queues = {}
playerturn = WHITE


def handle_recv(sock, addr):
    global playerturn
    while True:
        try:
            msg = tincanchat.recv_msg(sock, addr)
            print('Recv message:', msg)
            if msg == WHITE:
                clients[WHITE] = sock
            elif msg == BLACK:
                clients[BLACK] = sock
            else:
                tempcolor = None
                for color in clients.keys():
                    if clients[color] == sock:
                        tempcolor = color
                print(tempcolor)
                print(playerturn)
                if tempcolor == playerturn:
                    broad_cast(sock, msg)
                    if tempcolor == WHITE:
                        playerturn = BLACK
                    elif tempcolor == BLACK:
                        playerturn = WHITE
                else:
                    tincanchat.send_msg(sock, 'not your turn')
        except ConnectionError:
            print('Socket error !')
            playerturn = WHITE
            clients.clear()
            send_queues.clear()
            sock.close()

def broad_cast(sock, msg):
    with lock:
        for client in clients.values():
            if client != sock:
                send_queues[client.fileno()].put(msg)

def handle_send(sock, q, addr):

    while True:
        msg = q.get()
        if not msg:
            break
        try:
            tincanchat.send_msg(sock, msg)
        except ConnectionError:
            print('Client {} disconnected'.format(addr))
            sock.close()

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, PORT))
    s.listen(2)
    count_client = 1
    while True:
        client_sock, addr = s.accept()
        print('Client connected from: ', addr)
        q = queue.Queue()
        with lock:
            send_queues[client_sock.fileno()] = q



        send_thread = threading.Thread(target=handle_send,
                                       args=[client_sock, q, addr],
                                       daemon=True)
        recv_thread = threading.Thread(target=handle_recv,
                                       args=[client_sock, addr],
                                       daemon=True)
        recv_thread.start()
        send_thread.start()
