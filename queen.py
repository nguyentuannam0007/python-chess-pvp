import pygame
import help_function as hf
from piece import Piece

class Queen(Piece):
    def __init__(self, ai_settings, color='white'):
        super().__init__(ai_settings)
        if color.lower() == 'white':
            self.color = 'w'
            self.image = pygame.image.load('images\\pieces\\w-queen.png')
        elif color.lower() == 'black':
            self.color = 'b'
            self.image = pygame.image.load('images\\pieces\\b-queen.png')
        self.rect = self.image.get_rect()
        self.rect.w = ai_settings.width_piece
        self.rect.h = ai_settings.height_piece

        self.image = pygame.transform.smoothscale(self.image, (ai_settings.width_piece, ai_settings.height_piece))



    def findSquaresCanMove(self, board):
        self.squaresCanMove.clear()
        # Find all square in diagonal line
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, 1, 1)
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, -1, 1)
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, 1, -1)
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, -1, -1)
        # Find all square in straigth line
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, 1, 0)
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, 0, 1)
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, -1, 0)
        self.squaresCanMove += hf.findSquaresinLineVector(board.map_board, self, 0, -1)