import pygame

class Piece():
    def __init__(self, ai_settings):
        self.color = None
        self.image = None
        self.rect = None
        self.width_piece = ai_settings.width_piece
        self.height_piece = ai_settings.height_piece
        # Square where that pawn can move to it
        self.squaresCanMove = []
        # Square where that pawn passOver it
        self.squaresPassOver = []
        # Initilalize the current position
        self.x_pos = -1
        self.y_pos = -1

    def draw(self, screen):
        screen.blit(self.image, self.rect)

    def setPosition(self, x_pos, y_pos):
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.rect.x = x_pos * self.width_piece + self.rect.w
        self.rect.y = y_pos * self.height_piece


    def findSquaresCanMove(self, map_board):
        pass

    def getPosition(self):
        return [self.x_pos, self.y_pos]
