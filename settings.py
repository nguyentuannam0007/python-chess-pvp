import pygame


class Settings():
    def __init__(self):
        # Initialize w, h for piece
        self.width_piece = 90
        self.height_piece = 90
        # Initialize w, h for screen
        self.width_screen = (8 + 3) * self.width_piece
        self.height_screen = (8 + 1) * self.height_piece

        self.map_pieces = [
                           [0, 1, 0, 1, 0, 1, 0, 1],
                           [1, 0, 1, 0, 1, 0, 1, 0],
                           [0, 1, 0, 1, 0, 1, 0, 1],
                           [1, 0, 1, 0, 1, 0, 1, 0],
                           [0, 1, 0, 1, 0, 1, 0, 1],
                           [1, 0, 1, 0, 1, 0, 1, 0],
                           [0, 1, 0, 1, 0, 1, 0, 1],
                           [1, 0, 1, 0, 1, 0, 1, 0]
                        ]

ai_settings = Settings()