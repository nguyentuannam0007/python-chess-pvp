import pygame
import help_function as hf
from piece import Piece

class Knight(Piece):
    def __init__(self, ai_settings, color='white'):
        super().__init__(ai_settings)
        if color.lower() == 'white':
            self.color = 'w'
            self.image = pygame.image.load('images\\pieces\\w-knight.png')
        elif color.lower() == 'black':
            self.color = 'b'
            self.image = pygame.image.load('images\\pieces\\b-knight.png')
        self.rect = self.image.get_rect()
        self.rect.w = ai_settings.width_piece
        self.rect.h = ai_settings.height_piece

        self.image = pygame.transform.smoothscale(self.image, (ai_settings.width_piece, ai_settings.height_piece))




    def findSquaresCanMove(self, board):
        self.squaresCanMove.clear()
        x_pos, y_pos = self.x_pos, self.y_pos
        temp_squares = [[x_pos - 1, y_pos - 2], [x_pos + 1, y_pos - 2],
                        [x_pos - 2, y_pos - 1], [x_pos + 2, y_pos - 1],
                        [x_pos - 2, y_pos + 1], [x_pos + 2, y_pos + 1],
                        [x_pos - 1, y_pos + 2], [x_pos + 1, y_pos + 2]]
        for square in temp_squares:
            if square[0] > -1 and square[0] < len(board.map_board) and square[1] > -1 and square[1] < len(board.map_board):
                if board.map_board[square[1]][square[0]] != None:
                    if board.map_board[square[1]][square[0]].color != self.color:
                        self.squaresCanMove.append(square)
                else:
                    self.squaresCanMove.append(square)
