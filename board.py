import pygame
import help_function as hf
from pawn import Pawn
from rook import Rook
from bishop import Bishop
from queen import Queen
from knight import Knight
from king import King




class Board():



    def __init__(self, ai_settings):
        self.map_board = [
            [Rook(ai_settings, 'black'), Knight(ai_settings, 'black'), Bishop(ai_settings, 'black'),
             Queen(ai_settings, 'black'), King(ai_settings, 'black'),
             Bishop(ai_settings, 'black'), Knight(ai_settings, 'black'), Rook(ai_settings, 'black')],
            [Pawn(ai_settings, color='black'), Pawn(ai_settings, color='black'), Pawn(ai_settings, color='black'), Pawn(ai_settings, color='black'),
             Pawn(ai_settings, color='black'), Pawn(ai_settings, color='black'), Pawn(ai_settings, color='black'), Pawn(ai_settings, color='black')] ,
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [Pawn(ai_settings, color='white'), Pawn(ai_settings, color='white'), Pawn(ai_settings, color='white'), Pawn(ai_settings, color='white'),
             Pawn(ai_settings, color='white'), Pawn(ai_settings, color='white'), Pawn(ai_settings, color='white'), Pawn(ai_settings, color='white')] ,
            [Rook(ai_settings, 'white'), Knight(ai_settings, 'white'), Bishop(ai_settings, 'white'),
             Queen(ai_settings, 'white'), King(ai_settings, 'white'),
             Bishop(ai_settings, 'white'), Knight(ai_settings, 'white'), Rook(ai_settings, 'white')]
        ]
        self.map_pieces = ai_settings.map_pieces
        self.width_piece = ai_settings.width_piece
        self.height_piece = ai_settings.height_piece

        # Tile for board
        self.tile_white = pygame.transform.scale(
        pygame.image.load('images\\board\\tile-white.png'), (ai_settings.width_piece, ai_settings.height_piece))
        self.tile_back = pygame.transform.scale(
        pygame.image.load('images\\board\\tile-black-active.png'), (ai_settings.width_piece, ai_settings.height_piece))
        self.screen = None

        # the lastest piece move
        self.last_piece = None

    def draw(self, screen):
        self.drawMap(screen)
        self.drawPieces(screen)

    def init(self, screen):
        self.drawMap(screen)
        self.drawInitPieces(screen)

    def drawMap(self, screen):
        for i in range(len(self.map_pieces)):
            for j in range(len(self.map_pieces[i])):
                x_pos, y_pos = i * self.width_piece + self.width_piece, j * self.height_piece
                if self.map_pieces[i][j] == 0:
                    screen.blit(self.tile_white, (x_pos, y_pos, self.width_piece, self.height_piece))
                elif self.map_pieces[i][j] == 1:
                    screen.blit(self.tile_back, (x_pos, y_pos, self.width_piece, self.height_piece))

    def drawInitPieces(self, screen):
        for i in range(len(self.map_board)):
            for j in range(len(self.map_board[i])):
                x_pos, y_pos = j, i
                if self.map_board[i][j] != None:
                    self.map_board[i][j].setPosition(x_pos, y_pos)
                    self.map_board[i][j].draw(screen)
                    self.map_board[i][j].findSquaresCanMove(self)

    def drawPieces(self, screen):
        for row in self.map_board:
            for ele in row:
                if ele != None:
                    ele.draw(screen)

    def drawTile(self, screen, x_pos, y_pos):
        if (x_pos + y_pos) % 2 == 0:
            screen.blit(self.tile_white, (x_pos * self.width_piece + self.width_piece, y_pos * self.height_piece, self.width_piece, self.height_piece))
        else:
            screen.blit(self.tile_back, (x_pos * self.width_piece, y_pos * self.height_piece, self.width_piece, self.height_piece))

    def updateSquareCanMove(self):
        for row in self.map_board:
            for ele in row:
                if ele:
                    ele.findSquaresCanMove(self)

    def hastoMove(self, x_pos, y_pos, x_pdes, y_pdes):
        piece = self.map_board[y_pos][x_pos]
        canMove = False
        if piece:
            if [x_pdes, y_pdes] in piece.squaresCanMove:
                self.last_piece = piece
                # Handle enpassing pawn
                enpassing = False
                castling = False
                king = None
                direction = ''
                if isinstance(piece, Pawn):
                    if [x_pdes, y_pdes] in piece.squareEnpassing:
                        enpassing = True
                if isinstance(piece, King):
                    if [x_pdes, y_pdes] in piece.squaresCastling:
                        king = piece
                        if x_pdes == x_pos + king.dir_right:
                            direction = 'r'
                        elif x_pdes == x_pos + king.dir_left:
                            direction = 'l'
                        castling = True
                if enpassing:
                    piece.setPosition(x_pdes, y_pdes)
                    hf.swapPawnEnPassing(self.map_board, y_pos, x_pos, y_pdes, x_pdes, piece.direction)
                elif castling:
                    self.castling(king, direction)
                else:
                    piece.setPosition(x_pdes, y_pdes)
                    hf.swapTwoPiece(self.map_board, y_pos, x_pos, y_pdes, x_pdes)
                piece.squaresPassOver.append([x_pos, y_pos])
                canMove = True
        return canMove
    def isCheck(self, color):
        """Return True If King is checked else return False"""
        dangerous_move = self.getDangerousMove(color)
        king = self.findKing(color)
        attacker = self.findAttacker(king)
        if attacker:
            print('Check !')
            self.updateSafeMoveKing(king, attacker)
            return True
        return False

    def isCheckMate(self, color):
        checkmate = False
        if self.isCheck(color):
            checkmate = True
            king = self.findKing(color)
            attacker = self.findAttacker(king)
            # Kiểm tra king có thể đi đến chỗ an toàn nào nữa không
            if self.canEvade(king):  checkmate = False
            # Kiểm tra xem có ăn được con đang chiếu hay không
            if self.canCapture(king, attacker): checkmate = False
            # Kiểm tra có thể chặn chiếu được không
            if self.canBlockCheck(king, attacker): checkmate = False

        return checkmate

    def getDangerousMove(self, color):
        dangerous_move = []
        for row in self.map_board:
            for ele in row:
                if ele != None:
                    if ele.color != color:
                        for square in ele.squaresCanMove:
                            dangerous_move.append(square)
        return dangerous_move

    def canEvade(self, king):
        evade = False
        dangerous_move = self.getDangerousMove(king.color)
        for square in king.squaresCanMove:
            if square not in dangerous_move:
                evade = True
                break
        print('CanEvade: ' + str(evade))
        return evade
    def canCapture(self, kingisChecked, attacker):
        # Tìm ra king đã bị chiếu
        capture = False

        for row in self.map_board:
            for ele in row:
                if ele != None and ele.color == kingisChecked.color:
                    if attacker.getPosition() in ele.squaresCanMove:
                        print('canCapture: True')
                        return True
        print('canCapture: False')
        return False
    def canBlockCheck(self, king, attacker):
        blockCheck = False
        king_pos = king.getPosition()
        attacker_pos = attacker.getPosition()
        dx = king_pos[0] - attacker_pos[0]
        if dx != 0: dx = dx//abs(dx)
        dy = king_pos[1] - attacker_pos[1]
        if dy != 0: dy = dy//abs(dy)
        squareBlock = hf.findSquaresinLineVector(self.map_board, attacker, dx, dy)
        for square in squareBlock:
            print(square)
        for row in self.map_board:
            for ele in row:
                if ele != None and not isinstance(ele, King) and ele.color == king.color:
                    for square in ele.squaresCanMove:
                        if square in squareBlock:
                            print('canBlockCheck: True')
                            return True
        print('canBlockCheck: False')
        return False

    def findAttacker(self, king):
        attacker = None
        for row in self.map_board:
            for ele in row:
                if ele != None and ele.color != king.color:
                    if king.getPosition() in ele.squaresCanMove:
                        attacker = ele
        return attacker

    def findKing(self, color):
        king = None
        for row in self.map_board:
            for ele in row:
                if isinstance(ele, King) and ele.color == color:
                    king = ele
        return king

    def updateSafeMoveKing(self, king, attacker):
        # Update for the king
        for square in king.squaresCanMove:
            if square in attacker.squaresCanMove:
                king.squaresCanMove.remove(square)
        # Upate squaresCanMove for ally of the king
        king_pos = king.getPosition()
        attacker_pos = attacker.getPosition()
        dx = king_pos[0] - attacker_pos[0]
        if dx != 0: dx = dx // abs(dx)
        dy = king_pos[1] - attacker_pos[1]
        if dy != 0: dy = dy // abs(dy)
        squareSafe = hf.findSquaresinLineVector(self.map_board, attacker, dx, dy)
        squareSafe.append(attacker.getPosition())
        for row in self.map_board:
            for ele in row:
                if ele != None and not isinstance(ele, King) and ele.color == king.color:
                    for square in ele.squaresCanMove:
                        if not square in squareSafe:
                            ele.squaresCanMove.remove(square)
                elif ele != None and ele.color != king.color:
                    ele.findSquaresCanMove(self)


    def drawAvaiableSquare(self, screen, piece):
        for square in piece.squaresCanMove:
            circle_x = square[0] * piece.rect.w + piece.rect.w // 2 + piece.rect.w
            circle_y = square[1] * piece.rect.h + piece.rect.h // 2
            pygame.draw.circle(screen, (236,60,224), (circle_x, circle_y), 15, 0)

    # Method for handle castling

    def castling(self, king, direction):
        dir = 0
        if direction == 'r':
            dir = king.dir_right
        elif direction == 'l':
            dir = king.dir_left
        rook_cas = self.findRookCastling(king, direction)
        king_pos = king.getPosition()
        rook_pos = rook_cas.getPosition()
        # swap king
        hf.swapTwoPiece(self.map_board, king_pos[1], king_pos[0], king_pos[1], king_pos[0] + dir)
        king.setPosition(king_pos[0] + dir, king_pos[1])
        king_pos = king.getPosition()
        # swap rook
        hf.swapTwoPiece(self.map_board, rook_pos[1], rook_pos[0], king_pos[1], king_pos[0] - (dir // 2))
        rook_cas.setPosition(king_pos[0] - (dir // 2), king_pos[1])


    def canCastling(self, king, direction):
        castling = True
        rook_cas = self.findRookCastling(king, direction)
        if rook_cas == None and not isinstance(rook_cas, Rook): return False


        # Check first step of king and rook
        if len(king.squaresPassOver) != 0 or len(rook_cas.squaresPassOver) != 0: castling = False

        # Check path where from king to rook_cas is empty
        dx = -1
        if direction == 'r': dx = king.dir_right
        if direction == 'l': dx = king.dir_left

        squares_king_rook_cas = hf.findSquaresRankTwoPiece(self.map_board, king, rook_cas, int(dx/abs(dx)))
        if len(squares_king_rook_cas) == 0: castling = False

        for square in squares_king_rook_cas:
            if self.map_board[square[1]][square[0]] != None:
                castling = False
                break

        # Check for check king
        if self.isCheck(king.color): castling = False

        # Check for isChecked in path from king to rook
        # Use squares_king_rook_cas above
        dangerous_squares = self.getDangerousMove(king.color)
        for square in squares_king_rook_cas:
            if square in dangerous_squares:
                castling = False
                break
        # End check
        return castling
    def findRookCastling(self, king, direction):
        if king.color == 'w':
            if direction == 'r':
                return self.map_board[7][7]
            else:
                return self.map_board[7][0]
        elif king.color == 'b':
            if direction == 'r':
                return self.map_board[0][0]
            else:
                return self.map_board[0][7]

    def print(self):
        for row in self.map_board:
            for ele in row:
                if isinstance(ele, Pawn):
                    print('p', end=' ')
                elif isinstance(ele, Rook):
                    print('r', end=' ')
                elif isinstance(ele, Knight):
                    print('k', end=' ')
                elif isinstance(ele, Bishop):
                    print('b', end=' ')
                elif isinstance(ele, Queen):
                    print('q', end=' ')
                elif isinstance(ele, King):
                    print('k', end=' ')
                else:
                    print('0', end=' ')
            print('\n')
    def isGameOver(self):
        if self.isCheckMate('w') or self.isCheckMate('b'):
            return True
        return False

