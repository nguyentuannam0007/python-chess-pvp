import socket

BUFSIZE = 4096

def send_msg(sock, msg):

    msg = prep_msg(msg)
    try:
        sock.sendall(msg)
    except ConnectionError:
        print('Socket error')
        sock.close()
        return

def prep_msg(msg):
    msg += '\0'
    return msg.encode('utf-8')


def recv_msg(sock, addr):
    data = bytes()
    msg = ''
    while True:
        recvd = sock.recv(BUFSIZE)
        if not recvd:
            print('Client {} disconnected'.format(addr))
            break
        data += recvd
        if b'\0' in data:
            data = data.rstrip(b'\0')
            break
    msg = data.decode('utf-8')
    return msg


